// CSE 002 - 110 HW 02
// Arithmetic
// Shayra Delgado
// Must: Determine the total cost of each kind of item
//       Determine sales tax charged on each kind of item
//       Determine total cost of purchases, before tax
//       Determine total sales tax
//       Determine total paid for this transaction, including sales tax


public class Arithmetic{
  public static void main(String args[]){

    //Number of pairs of pants
        int numPants = 3;
    //Cost per pair of pants
        double pantsPrice = 34.98;
    //Number of sweatshirts
        int numShirts = 2;
    //Cost per shirt
        double shirtPrice = 24.99;
    //Number of belts
        int numBelts = 1;
    //Cost per belt
        double beltPrice = 33.99;
    //The tax rate
        double paSalesTax = 0.06;

    //Total cost of pants
        double totalCostofPants;
    //Total sales tax of pants
        double taxPants;
    //Total cost of sweatshirts
        double totalCostofShirts;
    //Total sales tax of shirts
        double taxShirts;
    //Total cost of belts
        double totalCostofBelts;
    //Total sales tax of belts
        double taxBelts;
    
    //Subtotal of purchases, before tax
        double subTotal;
    //Total sales tax
        double totalSalesTax;
    //Total of purchases, after tax
        double purchasesTotal;
    
    //Total cost of each kind of item
       //Pants
        totalCostofPants = numPants * pantsPrice;
       //Shirts
        totalCostofShirts = numShirts * shirtPrice;
       //Belts
        totalCostofBelts = numBelts * beltPrice;
    
    //Sales tax charged on each kind of item
       //Pants
        taxPants = totalCostofPants * paSalesTax;
       //Shirts
        taxShirts = totalCostofShirts * paSalesTax;
       //Belts
        taxBelts = totalCostofBelts * paSalesTax;
    
    //Total cost of purchases, before tax
        subTotal = totalCostofPants + totalCostofShirts + totalCostofBelts;
    
    //Total sales tax
        totalSalesTax = taxPants + taxShirts + taxBelts;
    
    //Total of purchases after tax
        purchasesTotal = subTotal + totalSalesTax;
    
    //Print out results
       //Total cost of each kind of item
        System.out.println(numPants + " pairs of pants cost " + (int)(totalCostofPants * 100)/100.0 + " dollars, in addition to " + (int)(taxPants * 100)/100.0 + " dollars of taxes");
        System.out.println(numShirts + " sweatshirts cost " + (int)(totalCostofShirts * 100)/100.0 + " dollars, in addition to " + (int)(taxShirts * 100)/100.0 + " dollars of taxes");
        System.out.println(numBelts + " belt cost " + (int)(totalCostofBelts * 100)/100.0 + " dollars, in addition to " + (int)(taxBelts * 100)/100.0 + " dollars of taxes");
       //Subtotal of purchases without tax
        System.out.println("The subtotal of all purchases without sales tax are " + (int)(subTotal * 100)/100.0 + " dollars");
       //Total sales tax
        System.out.println("The total sales tax on the purchases are " + (int)(totalSalesTax * 100)/100.0 + " dollars");
       //Total cost of purchases with tax
        System.out.println("The total of all purchases with sales tax is " + (int)(purchasesTotal * 100)/100.0 + " dollars");
    
    
  }
}
  