//CSE 002-010 Lab 02
//Cyclometer
//Shayra Delgado
//Interpret data given by a bicycle cyclometer
//Must: Print the number of minutes for each trip
//      Print the number of counts for each trip
//      Print the distance of each trip in miles 
//      Print the distance for the two trips combined

public class Cyclometer {
  // main method required for every Java program
  public static void main (String[] args){
    //our input data
    int secsTrip1= 480;      // Total time of trip 1 in seconds
    int secsTrip2= 3220;     // Total time of trip 2 in seconds
    int countsTrip1= 1561;   // Total rotation counts for Trip 1
    int countsTrip2= 9037;   // Total rotation counts for Trip 2
    
    // our intermediate variables and output data
    double wheelDiameter= 27.0;
    double PI= 3.14159;
    double feetPerMile= 5280;
    double inchesPerFoot= 12;
    double secondsPerMinute= 60;
    double distanceTrip1, distanceTrip2, totalDistance;
    
    //Print Out Stored Numbers
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + 
                      " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +
                      " minutes and had " + countsTrip2 + " counts.");
    
    //Computing Values for Distances and Conversion of inches, feet and mile
    distanceTrip1= countsTrip1*wheelDiameter*PI;
    distanceTrip1/= inchesPerFoot*feetPerMile;   
    distanceTrip2= countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance= distanceTrip1 + distanceTrip2;
    
    //Print Out Distances
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    

  } //end of main method
} //end of class