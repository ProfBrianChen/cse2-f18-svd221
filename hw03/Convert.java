// CSE 002 - 110 HW 03
// Program #1: Convert 
// Shayra Delgado
// Hurricanes drop a tremendous amount of water on areas of land.
// Must: Ask user for doubles for affected area in acres
//       Ask user for doubles for how many inches of rain were dropped on average
//       Convert the quantity of rain into cubic miles and print out the value for user

import java.util.Scanner;       //activates Scanner

public class Convert{                     //start of public class
  public static void main(String[] args){         //main method required for every program
  Scanner myScanner = new Scanner( System.in );       //declare an instant of the Sanner object to accept input
    System.out.print("Enter the affected area in acres: ");       //asks user for input of affected area in acres
    double affectedAcres = myScanner.nextDouble();        //accepts user input
    
    System.out.print("Enter the rainfall in the affected area: ");      //asks user for average rainfall
    double rainFallAvg = myScanner.nextDouble();          //accepts user input
    
    //declare variables are doubles
    double rainfallGallons;      
    double cubicMiles;
    
    //declare conversion formulas
    rainfallGallons = (affectedAcres * rainFallAvg) * 27154 ;       //converts acres and average rainfall to gallons of water
    cubicMiles = rainfallGallons * (0.000000000000908169);          //converts gallons of water to cubic miles
    
    System.out.println(cubicMiles + " cubic miles");            //prints out final calculated value in cubic miles
      
  }             //end of main method
}               //end of public class
  


