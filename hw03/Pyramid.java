// CSE 002 - 110 HW 03
// Pyramid
// Shayra Delgado
// Calculate volume of a squared-base pyramid
// Must: Ask user to input length of square side of the pyramid
//       Ask user to input height of pyramid
//       Calculate and print out volume of pyramid

import java.util.Scanner;       //activates Scanner

public class Pyramid{           //start of public class 
  public static void main(String[] args){               //start of main method
    Scanner myScanner = new Scanner( System.in );       //declare an instant of the Sanner object to accept input
    
    System.out.print("The square side of the pyramid is (input length): ");       //asks user for input of pyramid's square length
    double inputLength = myScanner.nextDouble();        //accepts user input
    
    System.out.print("The height of the pyramid is (input height): ");      //asks users for input of pyramid's height
    double inputHeight = myScanner.nextDouble();        //accepts user input
    
    //declare variables as doubles
    double pyramidArea = Math.pow(inputLength, 2);     //defines area with length squared
    double pyramidVolume;   
    
    pyramidVolume = (pyramidArea * inputHeight)/3;        //defines equation for volume of pyramid
    
    System.out.println("The volume inside the pyramid is: " + pyramidVolume + "." );     //prints out volume inside the pyramid
    
  }         //end of main method
}           //end of public class