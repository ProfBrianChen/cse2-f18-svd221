//CSE-002-010 lab03
//Shayra Vaneza Delgado
//Check
//Determine how much each person needs to spend in order to pay the check
//Must: Obtain the original cost of the check from the user
//      Obtain the percentage tip they wish to pay
//      Obtain the number of ways the check will be split
//      Print out the value that each person needs to spend

import java.util.Scanner;

public class Check{
  public static void main (String [] args){     //main method required for every Java program
    Scanner myScanner = new Scanner( System.in );       //declare an instant of the Sanner object to accept input
    System.out.print("Enter the original cost of the check in the form xx.xx: ");       //User input for the original cost of the check
    double checkCost = myScanner.nextDouble();        //accepts user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");     //User input for the tip percentage
    double tipPercent = myScanner.nextDouble();    //accepts user input
    tipPercent /= 100;    //Converts percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");      //User input for number of people at the dinner
    int numPeople = myScanner.nextInt();
    
    //declare all variables needed
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;      //stores digits 
    
    //declare all formulas or mathematical expressions
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;      //gets the whole amount, dropping decimal fraction
    
    //gets dimes and pennies amount right after dollar value
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    
    //Prints out final calculated value
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }         //end of main method
}         //end of class