// CSE 002 - 110 HW 01
// Welcome Class
// Shayra Delgado

public class WelcomeClass{
  public static void main(String args[]){
    //prints WelcomeClass
    System.out.println("   -------------");
    System.out.println("   |  WELCOME  |");
    System.out.println("   -------------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-S--V--D--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v ");                   
  }
}